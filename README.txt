組員:
	軟體二 蔡煜堃 411077005
	數三甲 林楷勛 410931103


專案包含以下檔案:

src => 資料庫定義語言、資料庫查詢語言、應用程式實作（部分完成）
	src\DDL => 建立各table的SQL語句
	src\DML => 滿足
		•Find the customer who has bought the most (by price) in the past year.
		•Find the top 2 products by dollar-amount sold in the past year.
		•Find those products that are out-of-stock at every store in Kaohsiung.
		•Find those packages that were not delivered within the promised time.
		•Assume the package shipped by USPS with tracking number 123456 is reported to have been destroyed in an accident. Find the contact information for the customer. Also, find the contents of that shipment and create a new shipment of replacement items.
		需求的程式包含SQL查詢語句實作
	src\application => 滿足
		•Customer service needs a lookup application to check inventory both locally and at nearby stores.
		•Call center staff need an application that allows quick access to customer data and the ability quickly to enter phone orders.
		•The stocking clerks at the warehouses need an application to help them record incoming shipments and update inventory.
		需求的程式包含SQL查詢語句實作（部分完成）
	src中的程式如要運行直接用IDE運行即可


ER-model => ER實體關係圖（中文）

Relational schema diagram => schema關係圖（中文）

queries result => 完成 The code you wrote for each of the listed queries and the result from running the queries. 需求

Table Inquire => 完成 Do NOT turn in a printout of all your data. Just run select count(*) on each of your tables so I can see how big they are without having to go online. 需求

DBTermProject111 => 原始需求文件



Thanks for your reading