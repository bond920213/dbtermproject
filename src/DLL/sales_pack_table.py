'''
Created on 2023年6月10日
銷售組合
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `sales_pack` (
    `serial_number` INT(11) NOT NULL,
    `discount` INT(11) NOT NULL,
    PRIMARY KEY (`serial_number`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()