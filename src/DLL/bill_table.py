'''
Created on 2023年6月10日
帳單
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `bill` (
    `agreement_serial_number` INT(16) NOT NULL,
    `serial_number` INT(16) NOT NULL,
    `cost` BIGINT(20) NOT NULL DEFAULT '0',
    `consumption_date` DATE NOT NULL,
    PRIMARY KEY (`agreement_serial_number`, `serial_number`) USING BTREE,
    CONSTRAINT `FK_bill_agreement` FOREIGN KEY (`agreement_serial_number`) REFERENCES `agreement` (`serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()