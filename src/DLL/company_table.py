'''
Created on 2023年6月10日
公司
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `company` (
    `name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    PRIMARY KEY (`name`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()