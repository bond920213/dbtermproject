'''
Created on 2023年6月10日
店家
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `shop` (
    `company_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `shop_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `address` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `area` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    PRIMARY KEY (`shop_name`) USING BTREE,
    INDEX `FK__company` (`company_name`) USING BTREE,
    CONSTRAINT `FK__company` FOREIGN KEY (`company_name`) REFERENCES `company` (`name`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()