'''
Created on 2023年6月10日
庫存
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `stock` (
    `shop_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `commodity_name` VARCHAR(200) NOT NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
    `commodity_serial_number` INT(11) NOT NULL DEFAULT '0',
    `amount` INT(16) UNSIGNED NOT NULL DEFAULT '0',
    `need_replenishment` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT '0',
    PRIMARY KEY (`shop_name`, `commodity_serial_number`, `commodity_name`) USING BTREE,
    INDEX `FK_stock_commodity` (`commodity_name`, `commodity_serial_number`) USING BTREE,
    CONSTRAINT `FK_stock_commodity` FOREIGN KEY (`commodity_name`, `commodity_serial_number`) REFERENCES `commodity` (`name`, `serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_stock_shop` FOREIGN KEY (`shop_name`) REFERENCES `shop` (`shop_name`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()