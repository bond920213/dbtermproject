'''
Created on 2023年6月10日
合同
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `agreement` (
    `serial_number` INT(16) NOT NULL,
    `signature_date` DATE NOT NULL,
    PRIMARY KEY (`serial_number`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()