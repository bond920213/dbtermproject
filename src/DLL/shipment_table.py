'''
Created on 2023年6月10日
出貨/配運
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `shipment` (
    `shipping_tracking_number` INT(20) NOT NULL,
    `commodity_name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_general_ci',
    `commodity_serial_number` INT(11) NOT NULL DEFAULT '0',
    `amount` INT(11) NOT NULL DEFAULT '1',
    INDEX `FK_shipment_sales_record` (`shipping_tracking_number`) USING BTREE,
    INDEX `FK_shipment_commodity` (`commodity_name`, `commodity_serial_number`) USING BTREE,
    CONSTRAINT `FK_shipment_commodity` FOREIGN KEY (`commodity_name`, `commodity_serial_number`) REFERENCES `commodity` (`name`, `serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_shipment_sales_record` FOREIGN KEY (`shipping_tracking_number`) REFERENCES `sales_record` (`shipping_tracking_number`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()