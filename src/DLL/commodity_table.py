'''
Created on 2023年6月10日
商品
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `commodity` (
    `name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_general_ci',
    `serial_number` INT(11) NOT NULL,
    `company_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `type` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `manufacturer` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `price` BIGINT(20) NOT NULL DEFAULT '0',
    PRIMARY KEY (`name`, `serial_number`) USING BTREE,
    INDEX `FK_commodity_company` (`company_name`) USING BTREE,
    CONSTRAINT `FK_commodity_company` FOREIGN KEY (`company_name`) REFERENCES `company` (`name`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()