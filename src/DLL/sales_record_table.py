'''
Created on 2023年6月10日
銷售紀錄
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `sales_record` (
    `buyer_account` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `address` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_general_ci',
    `contact_information` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `transport_company` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `shipping_tracking_number` INT(20) NOT NULL DEFAULT '0',
    `estimated_date_of_arrival` DATE NOT NULL,
    `sale_date` DATE NOT NULL,
    `shop_of_sale` VARCHAR(100) NOT NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
    `arrived` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `arrive_within_estimated_time` TINYINT(1) UNSIGNED NULL DEFAULT '0',
    `lost` TINYINT(1) UNSIGNED NULL DEFAULT '0',
    PRIMARY KEY (`shipping_tracking_number`) USING BTREE,
    INDEX `FK_sales_record_client` (`buyer_account`) USING BTREE,
    INDEX `FK_sales_record_cooperative_transport_company` (`transport_company`) USING BTREE,
    INDEX `FK_sales_record_shop` (`shop_of_sale`) USING BTREE,
    CONSTRAINT `FK_sales_record_client` FOREIGN KEY (`buyer_account`) REFERENCES `client` (`acount`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_sales_record_cooperative_transport_company` FOREIGN KEY (`transport_company`) REFERENCES `cooperative_transport_company` (`name`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_sales_record_shop` FOREIGN KEY (`shop_of_sale`) REFERENCES `shop` (`shop_name`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()