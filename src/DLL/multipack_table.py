'''
Created on 2023年6月10日
組合包
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `multipack` (
    `commodity_name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_general_ci',
    `commodity_serial_number` INT(11) NOT NULL DEFAULT '0',
    `pack_serial_number` INT(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`commodity_name`, `commodity_serial_number`, `pack_serial_number`) USING BTREE,
    INDEX `FK_multipack_sales_pack` (`pack_serial_number`) USING BTREE,
    CONSTRAINT `FK_multipack_commodity` FOREIGN KEY (`commodity_name`, `commodity_serial_number`) REFERENCES `commodity` (`name`, `serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_multipack_sales_pack` FOREIGN KEY (`pack_serial_number`) REFERENCES `sales_pack` (`serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()