'''
Created on 2023年6月10日
客戶
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """CREATE TABLE `client` (
    `name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `acount` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `register_company_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_general_ci',
    `credit_card_number` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
    `agreement_serial_number` INT(11) NULL DEFAULT NULL,
    PRIMARY KEY (`acount`) USING BTREE,
    INDEX `FK_client_company` (`register_company_name`) USING BTREE,
    INDEX `FK_client_agreement` (`agreement_serial_number`) USING BTREE,
    CONSTRAINT `FK_client_agreement` FOREIGN KEY (`agreement_serial_number`) REFERENCES `agreement` (`serial_number`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_client_company` FOREIGN KEY (`register_company_name`) REFERENCES `company` (`name`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;"""

cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()