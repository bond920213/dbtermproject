'''
Created on 2023年6月10日
Find those products that are out-of-stock at every store in Kaohsiung.
在高雄的每家商店找到那些缺貨的產品。
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """SELECT stock.commodity_name, shop.shop_name
FROM `411077005`.stock
LEFT JOIN `411077005`.shop ON stock.shop_name = shop.shop_name
WHERE stock.need_replenishment = 1 AND shop.area = '高雄'
ORDER BY shop.shop_name;"""

cursor.execute(sql)
conn.commit()

rows = cursor.fetchall()
# print(rows)
for row in rows:
    print(row)

cursor.close()
conn.close()

'''SELECT commodity_name, shop_name
FROM `411077005`.stock
WHERE need_replenishment = 1
ORDER BY shop_name;'''