'''
Created on 2023年6月10日
•    Assume the package shipped by USPS with tracking number 123456 is reported to have been destroyed in an accident. Find the contact information for the customer. Also, find the contents of that shipment and create a new shipment of replacement items.
假設由USPS 運送的跟踪號為123456 的包裹被報告在一次事故中被毀壞。 查找客戶的聯繫信息。 此外，找到該貨件的內容並創建新的替換物品貨件。
@author: bond9
'''
import mariadb
from datetime import date, timedelta

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """SELECT buyer_account, address, contact_information, transport_company, shop_of_sale, shipping_tracking_number
FROM `411077005`.sales_record
WHERE lost = 1 AND lost_processed <> 1"""

cursor.execute(sql)
conn.commit()

rows = cursor.fetchall()

sql0 = """SELECT name, contact_information
FROM `411077005`.sales_record
LEFT JOIN `411077005`.client ON sales_record.buyer_account = client.acount
WHERE lost = 1 AND lost_processed <> 1"""

cursor.execute(sql0)
conn.commit()

contact_information = cursor.fetchall()

for item in contact_information:
    print("包裹毀壞的客戶姓名：" + item[0] + " , 聯繫資料：　" + item[1])

sql1 = """UPDATE `411077005`.sales_record
SET lost_processed = 1
WHERE lost = 1"""

cursor.execute(sql1)
conn.commit()

# print(rows)
for row in rows:
    sql2 = """SELECT shipping_tracking_number + 1
    FROM `411077005`.sales_record
    ORDER BY shipping_tracking_number DESC
    LIMIT 1;"""
    cursor.execute(sql2)
    conn.commit()
    serial_number = cursor.fetchall()
    new_tracking_number = serial_number[0][0]
    current_date = date.today()
    estimated_date_of_arrival = current_date + timedelta(days=5)
    values = ""
    for item in row:
        if (type(item) == type(123)):
            old_tracking_number = item
        else:
            values += ", '" + item + "'"
    values += ", " + str(new_tracking_number)
    values += ", '" + str(current_date) + "'"
    values += ", '" + str(estimated_date_of_arrival) + "'"
    values = values.replace(", ", "", 1)
    # print(values)
    # print(old_tracking_number)
    # print(new_tracking_number)
    sql3 = """INSERT INTO `411077005`.`sales_record` (`buyer_account`, `address`, `contact_information`, `transport_company`, `shop_of_sale`, `shipping_tracking_number`, `sale_date`, `estimated_date_of_arrival`)
    VALUES (""" + values + ");"
    # print(sql3)
    cursor.execute(sql3)
    conn.commit()
    
    sql4 = """INSERT INTO `411077005`.shipment (`shipping_tracking_number`, commodity_name, commodity_serial_number, amount)
    SELECT """+str(new_tracking_number)+""", commodity_name, commodity_serial_number, amount
    FROM `411077005`.shipment
    WHERE shipping_tracking_number = """+str(old_tracking_number)+""";"""
    cursor.execute(sql4)
    conn.commit()
for item in contact_information:
    print("客戶：" + item[0] + " 的包裹已自動重新發貨")

cursor.close()
conn.close()