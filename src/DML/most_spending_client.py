'''
Created on 2023年6月10日
Find the customer who has bought the most (by price) in the past year.
找到過去一年購買最多（按價格）的客戶。
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """SELECT buyer_account, SUM(price*amount) AS total_spent
FROM `411077005`.sales_record
LEFT JOIN `411077005`.shipment ON sales_record.shipping_tracking_number = shipment.shipping_tracking_number
LEFT JOIN `411077005`.commodity ON shipment.commodity_name = commodity.name
WHERE sale_date >= DATE_SUB(CURDATE(), INTERVAL 1 YEAR) AND lost <> 1
GROUP BY buyer_account
ORDER BY total_spent DESC
LIMIT 1;"""

cursor.execute(sql)
conn.commit()

rows = cursor.fetchall()
converted_data = []
for item in rows:
    converted_data.append((item[0], int(item[1])))
print(converted_data)

cursor.close()
conn.close()