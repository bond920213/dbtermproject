'''
Created on 2023年6月10日
Find those packages that were not delivered within the promised time.
查找未在承諾時間內送達的包裹。
@author: bond9
'''
import mariadb

conn = mariadb.connect(**{
    "user": "411077005",
    "password": "411077005",
    "host": "140.127.74.226",
    "database": "411077005"
    })

cursor = conn.cursor()

sql = """SELECT buyer_account, address, contact_information, transport_company, shipping_tracking_number, estimated_date_of_arrival, sale_date, shop_of_sale
FROM (
    SELECT  *
    FROM `411077005`.sales_record
    WHERE arrived = 1 AND arrive_within_estimated_time <> 1
    
    UNION
    
    SELECT  *
    FROM `411077005`.sales_record
    WHERE lost = 1
) AS subquery;"""

cursor.execute(sql)
conn.commit()

rows = cursor.fetchall()
# print(rows)
for row in rows:
    print(row)

cursor.close()
conn.close()